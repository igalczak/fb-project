<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	
	protected function setupLayout()
	{		
		
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	protected function getConfig(){
		return array(
				'app_id'             => '1830171427244700',
				'app_secret'         => '7a3cbeebb0990c39450510d7c94993ce',
				'required_scope'     => 'public_profile, user_posts, user_photos, email, user_videos, pages_show_list',
				'redirect_url'       => 'http://fb.galczak.com.pl/',
		);
	}
}
