<?php

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRedirectLoginHelper;;

class HomeController extends BaseController {


	public function facebookAction()
	{
		error_reporting(E_ALL ^ E_NOTICE);
		ini_set('display_errors', '1');
		
		$config = $this->getConfig();
		session_start();
		FacebookSession::setDefaultApplication($config['app_id'], $config['app_secret']);
		
		$helper = new FacebookRedirectLoginHelper($config['redirect_url']);
		if (!$_SESSION['FB_session']){
			try {
				$_SESSION['FB_session'] = $helper->getSessionFromRedirect();
			} catch(FacebookRequestException $ex) {
				echo " Error : " . $ex->getMessage();
			} catch(\Exception $ex) {
				echo " Error : " . $ex->getMessage();
			}
			if ($_SESSION['FB_session']){
				header("Location: /");
				exit;
			}
		}
		
		
		$user = array();
		
		if ($_SESSION['FB_session']){
			$access_token = $_SESSION['FB_session']->getToken();

			$user['profile'] = (new FacebookRequest($_SESSION['FB_session'], 'GET', '/me'))->execute()->getGraphObject();
			$user['photos'] = (new FacebookRequest($_SESSION['FB_session'], 'GET', '/me/photos'))->execute()->getGraphObject();
			$user['posts'] = (new FacebookRequest($_SESSION['FB_session'], 'GET', '/me/posts'))->execute()->getGraphObject();
			
			$photos_obj = $user['photos']->getProperty('data');
			$photos = array();
			if ($photos_obj){
				for ($i = 0; $photos_obj->getProperty($i); $i++){
					$name = $photos_obj->getProperty($i)->getProperty('name');
					$photos[$i]['name'] = ($name? $name : 'undefined name');
					$id = $photos_obj->getProperty($i)->getProperty('id');
					$photos[$i]['path'] = "http://graph.facebook.com/$id/picture";
				}
			}
			
			$posts_obj = $user['posts']->getProperty('data');
			$posts = array();
			if ($posts_obj){
				for ($i = 0; $posts_obj->getProperty($i); $i++){
					$posts[$i]['created_time'] = $posts_obj->getProperty($i)->getProperty('created_time');
					$posts[$i]['story'] = $posts_obj->getProperty($i)->getProperty('story');
					$posts[$i]['message'] = $posts_obj->getProperty($i)->getProperty('message');
				}
			}
			$_SESSION['logoutUrl'] = $helper->getLogoutUrl( $_SESSION['FB_session'], $config['redirect_url'] );
		}else
			$logUrl = $helper->getLoginUrl(array('scope' => $config['required_scope']));

		
		return View::make('facebook', array(
				'user'=>$user,
				'logUrl'=>$logUrl,
				'photos'=>$photos,
				'posts'=>$posts,
		));
	}

	
	
	public function logoutAction(){
		session_start();
		header("Location: ".$_SESSION['logoutUrl']);
		$_SESSION = array();
		exit;
	}
}
