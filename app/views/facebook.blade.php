<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Facebook test</title>
	<link href="/css/materialize.css" rel="stylesheet">
</head>
<body>

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1830171427244700',
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>



	<div class="container row">
		@if ($user)
		<div class="col m12">
			<p>Jesteś zalogowany jako {{{ $user['profile']->getProperty('name') }}}</p>
			<a href="/logout">Wyloguj z Facebooka</a>
		</div>
		<div class="col m6">
			@if (!$photos)
			<h3>You havent public photos</h3>
			@else
			<h3>Photos</h3>
			<ul>
			@foreach ($photos as $photo)
				<li><a href="{{{ $photo['path'] }}}" target="_blanc">{{{ $photo['name'] }}}</a></li>
			@endforeach
			</ul>
			@endif
		</div>
		<div class="col m6">
			@if (!$posts)
			<h3>You havent public posts</h3>
			@else
			<h3>Posts</h3>
			<ul>
			@foreach ($posts as $post)
				<li>
					Created at: <span class="yellow light">{{{ $post['created_time'] }}}</span><br>
					@if ($post['story'])
					{{{ $post['story'] }}}<br>
					@endif
					@if ($post['message'])
					{{{ $post['message'] }}}<br>
					@endif
				</li>
				<div style="margin-top:10px; margin-bottom: 20px; border-top:1px red solid"></div>
			@endforeach
			</ul>
			@endif
		</div>
		
		@else
		<p>Nie jesteś zalogowany</p>
		<a href="{{{ $logUrl }}}">Zaloguj się przez Facebook</a>
		@endif
	</div>
	
	
</body>
</html>
